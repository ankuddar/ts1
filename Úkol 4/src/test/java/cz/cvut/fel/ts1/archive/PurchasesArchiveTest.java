package cz.cvut.fel.ts1.archive;


import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class PurchasesArchiveTest {

    private PurchasesArchive archive;
    private Order orderMock;
    private StandardItem standardItemMock;

    @BeforeEach
    void setUp() {
        orderMock = mock(Order.class);
        standardItemMock = mock(StandardItem.class);
        archive = new PurchasesArchive();

        ArrayList<Item> itemsList = new ArrayList<>(Collections.nCopies(5, standardItemMock));
        when(orderMock.getItems()).thenReturn(itemsList);
        when(standardItemMock.getID()).thenReturn(1);

    }

    @Test
    public void testPutOrderToPurchasesArchive() {

        archive.putOrderToPurchasesArchive(orderMock);

        assertEquals(5, archive.getHowManyTimesHasBeenItemSold(standardItemMock));
    }

    @Test
    public void testGetHowManyTimesHasBeenItemSold() {

        archive.putOrderToPurchasesArchive(orderMock);
        int soldTimes = archive.getHowManyTimesHasBeenItemSold(standardItemMock);

        assertEquals(5, soldTimes);
    }

    @Test
    public void testPrintItemPurchaseStatistics() {

        archive.putOrderToPurchasesArchive(orderMock);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();

        System.setOut(new PrintStream(outContent));
        archive.printItemPurchaseStatistics();

        assertTrue(outContent.toString().contains("ITEM PURCHASE STATISTICS:"));
        assertTrue(outContent.toString().contains("HAS BEEN SOLD 5 TIMES"));
        System.setOut(System.out);
    }

    @Test
    public void testItemPurchaseArchiveEntryConstructor() {
        Item testItem = mock(Item.class);
        when(testItem.getID()).thenReturn(10);
        when(testItem.toString()).thenReturn("Test Item");

        ItemPurchaseArchiveEntry archiveEntry = new ItemPurchaseArchiveEntry(testItem);

        assertEquals(1, archiveEntry.getCountHowManyTimesHasBeenSold());
        assertEquals("ITEM  Test Item   HAS BEEN SOLD 1 TIMES", archiveEntry.toString());
    }
}
