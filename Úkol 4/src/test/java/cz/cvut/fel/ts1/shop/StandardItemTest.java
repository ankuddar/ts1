package cz.cvut.fel.ts1.shop;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

class StandardItemTest {

    private static StandardItem standardItem;

    @BeforeAll
    static void setUp() {
        int id = 1;
        String name = "Test Item";
        float price = 19.99f;
        String category = "Test Category";
        int loyaltyPoints = 100;
        standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
    }

    @Test
    @DisplayName("Testing StandardItem constructor.")
    public void testStandardItemConstructor() {

        assertEquals(1, standardItem.getID(), "ID should match the input value");
        assertEquals("Test Item", standardItem.getName(), "Name should match the input value");
        assertEquals(19.99f, standardItem.getPrice(), "Price should match the input value");
        assertEquals("Test Category", standardItem.getCategory(), "Category should match the input value");
        assertEquals(100, standardItem.getLoyaltyPoints(), "Loyalty points should match the input value");
    }

    @Test
    @DisplayName("Testing copy() method of StandardItem class.")
    public void testCopyMethod() {

        StandardItem copiedItem = standardItem.copy();

        assertEquals(standardItem.getID(), copiedItem.getID(), "Copied item ID should match the original");
        assertEquals(standardItem.getName(), copiedItem.getName(), "Copied item name should match the original");
        assertEquals(standardItem.getPrice(), copiedItem.getPrice(), "Copied item price should match the original");
        assertEquals(standardItem.getCategory(), copiedItem.getCategory(), "Copied item category should match the original");
        assertEquals(standardItem.getLoyaltyPoints(), copiedItem.getLoyaltyPoints(), "Copied item loyalty points should match the original");
        assertEquals(standardItem, copiedItem, "Copied item should be equal to the original");
    }

    @ParameterizedTest
    @DisplayName("Testing equals() method of StandardItem class.")
    @CsvFileSource(
            resources = "/parametersForStandardItemEqualsMethod.csv",
            numLinesToSkip = 1
    )
    void testEqualsMethod(int id1, String name1, float price1, String category1, int loyaltyPoints1,
                          int id2, String name2, float price2, String category2, int loyaltyPoints2,
                          boolean expectedResult) {

        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);

        assertEquals(expectedResult, item1.equals(item2), "The equals method should work as expected based on CSV data");
    }

    @Test
    @DisplayName("Testing equals method with incorrect type expecting no exception.")
    public void testEqualsMethodWithIncorrectType() {
        Object wrongType = new Object();

        assertFalse(standardItem.equals(wrongType), "Equals method should return false when comparing with an object of incorrect type.");
    }

}