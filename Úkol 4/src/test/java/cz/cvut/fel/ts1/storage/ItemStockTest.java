package cz.cvut.fel.ts1.storage;


import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    Item item;
    ItemStock itemStock;

    @BeforeEach
    void setUp() {
        item = new StandardItem(2424, "Rohlik", 2.70f, "Pecivo", 2);
    }


    @Test
    @DisplayName("Testing ItemStock Constructor.")
    public void testItemStockConstructor() {

        itemStock = new ItemStock(item);

        assertEquals(item, itemStock.getItem());

        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @DisplayName("Testing IncreaseItemCount() method of ItemStock class.")
    @ValueSource(ints = {1, 5, 10})
    void testIncreaseItemCount(int increment) {

        ItemStock stock = new ItemStock(item);

        stock.IncreaseItemCount(increment);

        assertEquals(increment, stock.getCount(), "Count should increase by the increment value");
    }

    @ParameterizedTest
    @DisplayName("Testing DecreaseItemCount() method of ItemStock class.")
    @ValueSource(ints = {1, 5, 10})
    void testDecreaseItemCount(int decrement) {

        ItemStock stock = new ItemStock(item);

        stock.IncreaseItemCount(20);
        stock.decreaseItemCount(decrement);

        assertEquals(20 - decrement, stock.getCount(), "Count should decrease by the decrement value");
    }

}