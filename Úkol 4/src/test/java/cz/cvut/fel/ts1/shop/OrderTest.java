package cz.cvut.fel.ts1.shop;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.annotation.IncompleteAnnotationException;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    @Test
    @DisplayName("Testing Order constructor with all parameters.")
    void testOrderConstructorWithAllParameters() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1212, "Mrkev", 20.50f, "Zelenina", 5));
        Order order = new Order(cart, "Daria Ankudimova", "123 Wall Street", 1);

        assertEquals(cart.getCartItems(), order.getItems(), "Items should match the shopping cart contents");
        assertEquals("Daria Ankudimova", order.getCustomerName(), "Customer name should match");
        assertEquals("123 Wall Street", order.getCustomerAddress(), "Customer address should match");
        assertEquals(1, order.getState(), "State should be as set in constructor");
    }

    @Test
    @DisplayName("Testing Order constructor with default state.")
    void testOrderConstructorWithDefaultState() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(2424, "Rohlik", 2.70f, "Pecivo", 2));
        Order order = new Order(cart, "Jarda Nowak", "456 Vaclavak");

        assertEquals(cart.getCartItems(), order.getItems(), "Items should match the shopping cart contents");
        assertEquals("Jarda Nowak", order.getCustomerName(), "Customer name should match");
        assertEquals("456 Vaclavak", order.getCustomerAddress(), "Customer address should match");
        assertEquals(0, order.getState(), "State should be default (0)");
    }

    @Test
    @DisplayName("Order constructor should handle null ShoppingCart.")
    void testConstructorWithNullCart() {

        Exception exception = assertThrows(NullPointerException.class, () -> {
            new Order(null, "Customer Name", "Customer Address", 1);
        });

        assertEquals("Cannot invoke \"cz.cvut.fel.ts1.shop.ShoppingCart.getCartItems()\" because \"cart\" is null", exception.getMessage());
    }


    @Test
    @DisplayName("Order constructor should handle null customerName and customerAddress.")
    void testConstructorWithNullCustomerDetails() {

        ShoppingCart cart = new ShoppingCart();
        Order order = new Order(cart, null, null);

        assertNull(order.getCustomerName(), "Customer name should be null");
        assertNull(order.getCustomerAddress(), "Customer address should be null");
        assertNotNull(order.getItems(), "Items list should not be null");

        System.out.println("System does not explicitly include logic for handling with nulls in customer details.");
    }
}