package cz.cvut.fel.ts1.shop;


import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.storage.NoItemInStorage;
import cz.cvut.fel.ts1.storage.Storage;
import org.junit.jupiter.api.*;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class EShopControllerTest {

    private Storage mockStorage;
    private PurchasesArchive mockArchive;
    private ShoppingCart cart;
    private EShopController controller;

    @BeforeEach
    void setUp() {

        mockStorage = mock(Storage.class);
        mockArchive = mock(PurchasesArchive.class);
        cart = new ShoppingCart();
        
        EShopController.setStorage(mockStorage);
        EShopController.setArchive(mockArchive);

    }

    @Test
    @DisplayName("Testing successful purchase.")
    void testSuccessfulPurchase() {
        try {
            cart.addItem(new StandardItem(1, "Test Item", 10.0f, "Category", 5));
            assertEquals(1, cart.getItemsCount());

            cart.addItem(new StandardItem(2, "Test Item", 20.0f, "Category", 5));
            assertEquals(2, cart.getItemsCount());

            cart.removeItem(2);
            assertEquals(1, cart.getItemsCount());

            assertEquals(10.0f, cart.getTotalPrice());

            EShopController.purchaseShoppingCart(cart, "John Doe", "123 Main St");

            verify(mockStorage).processOrder(any(Order.class));
            verify(mockArchive).putOrderToPurchasesArchive(any(Order.class));


            assertTrue(cart.getCartItems().isEmpty(), "Cart should be empty after purchase");
        } catch(Exception e) {
            fail(e);
        }
    }

    @Test
    @DisplayName("Testing purchase when out of stock.")
    void testItemOutOfStockSimplified() {
        try {
            cart.addItem(new StandardItem(2, "Out of Stock Item", 20.0f, "Category", 0));

            doThrow(new NoItemInStorage()).when(mockStorage).processOrder(any(Order.class));

            assertThrows(NoItemInStorage.class, () -> {
                EShopController.purchaseShoppingCart(cart, "Jane Doe", "456 Elm St");
            });
        } catch(Exception e) {
            fail(e);
        }
    }


}