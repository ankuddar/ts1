package pages;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class SignupLoginPage {
    private final WebDriver driver;

    @FindBy(id = "login-box-email")
    private WebElement loginEmailField;

    @FindBy(id = "login-box-pw")
    private WebElement loginPasswordField;

    @FindBy(css = ".btn[title='Log in']")
    private WebElement loginFormSubmitButton;

    @FindBy(id = "first-name")
    private WebElement signupFirstNameField;

    @FindBy(id = "last-name")
    private WebElement signupLastNameField;

    @FindBy(id = "email-address")
    private WebElement signupEmailField;

    @FindBy(id = "password")
    private WebElement signupPasswordField;

    @FindBy(id = "password-confirm")
    private WebElement signupPasswordConfirmField;

    @FindBy(css = ".btn[title='Create account']")
    private WebElement signupFormSubmitButton;

    public SignupLoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        closeCookies();
        closePopup();
    }

    public SignupLoginPage(WebDriver driver, String queryString) {
        this.driver = driver;
        driver.get(queryString);
        PageFactory.initElements(driver, this);
        closeCookies();
        closePopup();
    }

    public void setLoginEmail(String email) {loginEmailField.sendKeys(email);
    }

    public void setLoginPassword(String password) {loginPasswordField.sendKeys(password);
    }

    public void setSignupFirstName(String firstName) {
        signupFirstNameField.sendKeys(firstName);
    }

    public void setSignupLastName(String lastName) {
        signupLastNameField.sendKeys(lastName);
    }

    public void setSignupEmail(String email) {
        signupEmailField.sendKeys(email);
    }

    public void setSignupPassword(String password) {
        signupPasswordField.sendKeys(password);
    }

    public void setSignupPasswordConfirm(String password) {
        signupPasswordConfirmField.sendKeys(password);
    }

    public void sendLoginForm() {
        loginFormSubmitButton.click();
    }
    public void sendSignupForm() {
        Actions actions = new Actions(driver);
        actions.moveToElement(signupFormSubmitButton).click().perform();
    }

    public void closeCookies() {
        try {
            driver.findElement(By.cssSelector("button[data-cc-action='accept']")).click();
        } catch (NoSuchElementException ignored) {
        }
    }

    public void closePopup() {
        try {
            WebElement window = driver.findElement(By.className("QSIWebResponsive"));
            List<WebElement> buttons = window.findElements(By.cssSelector("button"));
            for (WebElement button : buttons) {
                if (button.getText().equals("No Thanks")) {
                    button.click();
                    break;
                }
            }


        } catch (NoSuchElementException ignored) {
        }
    }
}
