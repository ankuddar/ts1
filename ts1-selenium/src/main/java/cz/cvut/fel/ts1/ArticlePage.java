package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ArticlePage {
    private final WebDriver driver;
    @FindBy(className = "c-article-title")
    private WebElement articleTitle;
    @FindBy(css = ".c-article-header time")
    private WebElement publicationDate;

    @FindBy(css = ".c-bibliographic-information__list-item--doi .c-bibliographic-information__value")
    private WebElement doi;

    public ArticlePage (WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements ( driver,this );
        closeCookies ();
        closePopup ();
    }

    public String getArticleTitle () {
        return articleTitle.getText ();
    }

    public String getPublicationDate () {
        return publicationDate.getText ();
    }

    public String getDOI () {
        return doi.getText ();
    }
    private void closeCookies() {
        try {
            driver.findElement(By.cssSelector("button[data-cc-action='accept']")).click();
        } catch (NoSuchElementException ignored) {}
    }
    private void closePopup() {
        try {
            WebElement window = driver.findElement(By.className("QSIWebResponsive"));
            List<WebElement> buttons = window.findElements(By.cssSelector("button"));
            for (WebElement button : buttons) {
                if (button.getText().equals("No Thanks")) {
                    button.click();
                    break;
                }
            }
        } catch (NoSuchElementException ignored) {}
    }
}
