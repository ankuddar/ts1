package java;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class ProcessTest {
    private static WebDriver driver;

    @BeforeAll
    public static void setUpDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setPosition(new Point(0, 0));
        driver.manage().window().setSize(new Dimension(1920, 1080));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
    }

    @AfterAll
    public static void tearDownDriver() {
        if (driver != null) {
            driver.quit();
        }
    }



    @Test
    @Order(1)
    public void prepareData() {
        HomePage homePage = new HomePage(driver, "https://link.springer.com/");
        homePage.clickAdvancedSearchLink();
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver, driver.getCurrentUrl());
        advancedSearchPage.setAllWords("Page Object Model");
        advancedSearchPage.setLeastWords("Selenium Testing");
        advancedSearchPage.setDateFacetModeToIn();
        advancedSearchPage.setFacetStartYear(String.valueOf(LocalDate.now().getYear()));
        advancedSearchPage.sendForm();

        SearchPage searchPage = new SearchPage(driver, driver.getCurrentUrl());
        searchPage.setContentType("Article");

        ArrayList<String> links = searchPage.getResultsLinks(4);

        StringBuilder dataForCSV = new StringBuilder();

        for (String link : links) {
            ArticlePage page = new ArticlePage(driver, link);
            dataForCSV.append(page.getName()).append(", ");
            dataForCSV.append(page.getDate()).append(", ");
            dataForCSV.append(page.getDOI()).append("\n");
        }



        // CSV file
        try {
            FileWriter myWriter = new FileWriter("src/test/resources/data.csv");
            myWriter.write(dataForCSV.toString());
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Order(2)
    public void parameterizedTest() {
        // Here you can retrieve the data from the CSV file or use the stored data

        // Home
        HomePage homePage = new HomePage(driver, "https://link.springer.com/");
        homePage.clickRegisterLink();

        // Login
        SignupLoginPage signupLoginPage = new SignupLoginPage(driver);
        signupLoginPage.setLoginEmail("dasha_456789101112@mail.ru");
        signupLoginPage.setLoginPassword("ahsad600686D!");
        signupLoginPage.sendLoginForm();

        homePage.clickAdvancedSearchLink();
        // Search
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);

        advancedSearchPage.setTitleIsWords("Title");
        advancedSearchPage.sendForm();

        // Get
        SearchPage searchPage = new SearchPage(driver);

        //Article
        ArticlePage articlePage = new ArticlePage(driver, searchPage.getResultsLinks(1).get(0));
        //ASSERT
        assertEquals("Expected Name", articlePage.getName());
        assertEquals("Expected Date", articlePage.getDate());
        assertEquals("Expected DOI", articlePage.getDOI());
    }
}
