package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.SearchPage;
import java.time.Duration;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class SearchPageTest {
    WebDriver driver;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setPosition(new Point(0,0));
        driver.manage().window().setSize(new Dimension(1920,1080));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
    }

    @Test
    public void noResultsMessageTest() {
        //ARRANGE
        String expectedMessage = "No results found.";
        //ACT
        SearchPage searchPage = new SearchPage(driver, "https://link.springer.com/search/page/1?date-facet-mode=in&facet-start-year=2022&showAll=true&query=Page+AND+Object+AND+Model+AND+%28Sellenium+OR+Testing%29");
        searchPage.setPage(999); // Go to a page with no results

        //ASSERT
        assertTrue(searchPage.isNoResultsMessageDisplayed());
        assertEquals(expectedMessage, searchPage.getNoResultsMessage());
    }


    @AfterEach
    public void tearDown() {
        driver.quit();
    }
}
