package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        WebDriver driver=new ChromeDriver();

        driver.get( "https://moodle.fel.cvut.cz/login/index.php" );

        driver.manage().window().maximize();
        driver.findElement( By.xpath( "//*[@id=\"sso-form\"]/a" ) ).click();


        WebElement loginWait=new WebDriverWait( driver,Duration.ofSeconds( 5 ) ).until( ExpectedConditions.elementToBeClickable( By.cssSelector( "#cvut-login-form > form > fieldset > div > div > div > div.col.col-xs-12.col-sm-5 > button" ) ) );
        driver.findElement( By.id( "username" ) ).sendKeys( "ankuddar" );
        driver.manage().timeouts().implicitlyWait( Duration.ofMillis( 300 ) );

        driver.findElement( By.id( "password" ) ).sendKeys( "ahsad600686D!" );

        driver.findElement( By.cssSelector( "#cvut-login-form > form > fieldset > div > div > div > div.col.col-xs-12.col-sm-5 > button" ) ).click();

        WebElement softwareTestingClick=new WebDriverWait( driver,Duration.ofSeconds( 10 ) ).until( ExpectedConditions.elementToBeClickable( By.cssSelector( "a[href$='/course/view.php?id=8535']" ) ) );

        driver.findElement( By.cssSelector( "a[href$='/course/view.php?id=8535']" ) ).click();

        driver.findElement( By.xpath( "/html/body/div[2]/div[2]/div/div[2]/div/section/div/div/div/ul/li[5]/div[2]/ul/li[16]/div/div[2]/div/div/div/div/a" ) ).click();

        driver.findElement( By.cssSelector( "button[type='submit']" ) ).click();

        driver.findElement( By.cssSelector( "input[value='Start attempt']" ) ).click();

     //test
        driver.findElement( By.cssSelector( "textarea" ) ).sendKeys( "Darya Ankudimova 103" );

        driver.findElement( By.cssSelector( "input[type='text']" ) ).sendKeys( "86400" );

        Select selectQ1=new Select( driver.findElement( By.cssSelector( "div:nth-child(3) > div > div > div > p > span > select" ) ) );
        selectQ1.selectByVisibleText( "Oberon" );
        Select selectQ2=new Select( driver.findElement( By.cssSelector( "div:nth-child(4) > div > div > div > p > span > select" ) ) );
        selectQ2.selectByVisibleText( "Rumunsko" );

        WebElement testEndedScreen=new WebDriverWait( driver,Duration.ofSeconds( 20 ) ).until( ExpectedConditions.elementToBeClickable( By.xpath( "/html/body/div[2]/div[2]/div/div[2]/div/section/div[2]/div/a" ) ) );

        driver.findElement( By.xpath( "//*[@id=\"action-menu-toggle\"]" ) ).click();
        driver.findElement( By.xpath( "/html/body/div[2]/header/div/div[2]/div/div[1]/div[1]/div/div/div/div/div/div/a[6]" ) ).click();

     //odchazeni
        driver.findElement( By.cssSelector( "button[class='btn btn-primary']" ) ).click();
    }
}